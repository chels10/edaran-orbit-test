<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class UserKinModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'user_kin';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'user_id','kin_name','kin_address','kin_postcode','kin_district','kin_state','kin_contact','kin_relation'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'kin_name',
        'kin_address',
        'kin_contact',
        'kin_relation'
    ];
}
