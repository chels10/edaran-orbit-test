<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DistrictModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_district';
    protected $primaryKey = 'id';

    // public $incrementing = false;

    protected $fillable = [
    	'id', 'district_name', 'state_id', 'country_id'
    ];


    protected $auditInclude = [
        'id',
        'district_name',
        'state_id',
    ];

    public function state(){
        return $this->belongsTo('App\StateModel');
    }

    // public function getStatenameAttribute(){
    //     return $this->attributes['statename'];
    // }

    // public function setStatenameAttribute($state){
    //     $this->attributes['statename'] = $state;
    // }

}
