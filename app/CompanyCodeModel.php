<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CompanyCodeModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'y_companycode';
    protected $primaryKey = 'company_code';

    protected $fillable = [
    	'company_code', 'company_name', 'postal_code', 'district_id', 'state_id', 'country_id', 'office_phone', 'mobile_phone', 'fax', 'email', 'remark', 'created_at', 'updated_at'
    ];

        /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'company_code', 'company_name', 'postal_code', 'district_id', 'state_id', 'country_id', 'office_phone', 'mobile_phone', 'fax', 'email', 'remark', 'created_at', 'updated_at', 'created_by', 'updated_by',
    ];


    public function district(){
        return $this->belongsTo('App\DistrictModel', 'district_id');
    }

    public function state(){
        return $this->belongsTo('App\StateModel', 'state_id');
    }

}
