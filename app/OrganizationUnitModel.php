<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class OrganizationUnitModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'y_organizationunit';
    protected $primaryKey = 'orgunit_code';

    protected $fillable = [
    	'orgunit_code', 'orgunit_name', 'address', 'postal_code', 'district_id', 'state_id', 'country_id', 'office_phone', 'mobile_phone', 'fax', 'email', 'remark'
    ];

        /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'orgunit_code', 'orgunit_name', 'address', 'postal_code', 'district_id', 'state_id', 'country_id', 'office_phone', 'mobile_phone', 'fax', 'email', 'remark',
    ];

    public function district(){
        return $this->belongsTo('App\DistrictModel', 'district_id');
    }

    public function state(){
        return $this->belongsTo('App\StateModel', 'state_id');
    }

}
