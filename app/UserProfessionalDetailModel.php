<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class UserProfessionalDetailModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'user_professional_detail';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'user_id','staff_no','date_joined','job_grade','designation','unit_base',
      'branch_id','date_exited',
      'department_id','unit_id','company_code_id','organization_unit_code_id'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'user_id',
        'staff_no',
        'date_joined',
        'job_grade',
        'unit_base'
    ];
}
