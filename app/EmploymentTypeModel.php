<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class EmploymentTypeModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_employment_type';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'id', 'employment_type_name', 'created_at', 'updated_at'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'employment_type_name',
    ];
}
