<?php

namespace App\Http\Controllers;

use App\DepartmentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = DepartmentModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id',
            'dept_code',
            'dept_name',

        */

        $validator = Validator::make($request->all(), [
            //'id' => 'required|integer',
            'dept_code' => 'required|max:100',
            'dept_name' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $dept_code = $request->get('dept_code');
        $dept_name = $request->get('dept_name');

        $model = new DepartmentModel();
        $model->fill([
            'dept_code' => $dept_code,
            'dept_name' => $dept_name
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function show(DepartmentModel $departmentModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = DepartmentModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DepartmentModel $departmentModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'dept_code' => 'required|max:100',
            'dept_name' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $dept_code = $request->get('dept_code');
        $dept_name = $request->get('dept_name');

        $model = DepartmentModel::find($id);
        if (!$model) {
            return json_encode(['Response' => '400']);
        }

        $model->fill([
            'dept_code' => $dept_code,
            'dept_name' => $dept_name
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(DepartmentModel $departmentModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = DepartmentModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
