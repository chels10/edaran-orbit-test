<?php

namespace App\Http\Controllers;

use App\UserProfileModel;
use Illuminate\Http\Request;

class UserProfileModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserProfileModel  $userProfileModel
     * @return \Illuminate\Http\Response
     */
    public function show(UserProfileModel $userProfileModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserProfileModel  $userProfileModel
     * @return \Illuminate\Http\Response
     */
    public function edit(UserProfileModel $userProfileModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserProfileModel  $userProfileModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserProfileModel $userProfileModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserProfileModel  $userProfileModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserProfileModel $userProfileModel)
    {
        //
    }
}
