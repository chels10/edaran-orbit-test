<?php

namespace App\Http\Controllers;

use App\OrganizationModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrganizationController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /*
        Purpose : To retrieve all - TEST Purpose
    */
    public function retrieveall(){
        $result = OrganizationModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
            'id',
            'organization_name',
            'postcode',
            'district_id',
            'state_id',
            'country_id',
            'office_phone',
            'mobile_phone',
            'fax',
            'email',
            'remark'
        */

        $validator = Validator::make($request->all(), [
            'organization_name' => 'required|max:100',
            'postal_code' => 'required|max:50',
            'district_id' => 'required|integer',
            'state_id' => 'required|integer',
            'country_id' => 'required|integer',
            'office_phone' => 'required|max:50',
            'mobile_phone' => 'required|max:50',
            'fax' => 'required|max:50',
            'email' => 'required|max:100',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $organization_name = $request->get('organization_name');
        $address = $request->get('address');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $country_id = $request->get('country_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = new OrganizationModel();
        $model->fill([
            'organization_name' => $organization_name,
            'address' => $address,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'country_id' => $country_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizationModel  $organizationModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, OrganizationModel $organizationModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = OrganizationModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationModel  $organizationModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizationModel $organizationModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'organization_name' => 'required|max:100',
            'postal_code' => 'required|max:50',
            'district_id' => 'required|integer',
            'state_id' => 'required|integer',
            'office_phone' => 'required|max:50',
            'mobile_phone' => 'required|max:50',
            'fax' => 'required|max:50',
            'email' => 'required|max:100',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $organization_name = $request->get('organization_name');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = OrganizationModel::find($id);
        $model->fill([
            'organization_name' => $organization_name,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationModel  $organizationModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, OrganizationModel $organizationModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = OrganizationModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
