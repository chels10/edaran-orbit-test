<?php

namespace App\Http\Controllers;

use App\CompanyCodeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CompanyCodeController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /*
        Purpose : To retrieve all - TEST Purpose
    */
    public function retrieveall(){
        $result = CompanyCodeModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        /*
            'company_code',
            'company_name',
            'postcode',
            'district_id',
            'state_id',
            'country_id',
            'office_phone',
            'mobile_phone',
            'fax',
            'email',
            'remark'
        */

        $validator = Validator::make($request->all(), [
            //'company_code' => 'required|integer',
            'company_name' => 'required|max:100',
            'postal_code' => 'required|max:10',
            'district_id' => 'required|integer',
            'state_id' => 'required|integer',
            //'country_id' => 'required|integer',
            'office_phone' => 'required|max:50',
            'mobile_phone' => 'required|max:50',
            'fax' => 'required|max:50',
            'email' => 'required|max:150',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $company_name = $request->get('company_name');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $country_id = $request->get('country_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = new CompanyCodeModel();
        $model->fill([
            'company_name' => $company_name,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'country_id' => $country_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyCodeModel  $companyCodeModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, CompanyCodeModel $companyCodeModel){

        $validator = Validator::make($request->all(), [
            'company_code' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('company_code');
        $result = CompanyCodeModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyCodeModel  $companyCodeModel
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyCodeModel $companyCodeModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyCodeModel  $companyCodeModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyCodeModel $companyCodeModel)
    {

        $validator = Validator::make($request->all(), [
            'company_code' => 'required|integer',
            'company_name' => 'required|max:100',
            'postal_code' => 'required|max:10',
            'district_id' => 'required|integer',
            'state_id' => 'required|integer',
            //'country_id' => 'required|integer',
            'office_phone' => 'required|max:50',
            'mobile_phone' => 'required|max:50',
            'fax' => 'required|max:50',
            'email' => 'required|max:150',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $company_code = $request->get('company_code');
        $company_name = $request->get('company_name');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $country_id = $request->get('country_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = CompanyCodeModel::find($company_code);
        $model->fill([
            'company_name' => $company_name,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'country_id' => $country_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyCodeModel  $companyCodeModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CompanyCodeModel $companyCodeModel)
    {

        $validator = Validator::make($request->all(), [
            'company_code' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('company_code');
        $result = CompanyCodeModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
