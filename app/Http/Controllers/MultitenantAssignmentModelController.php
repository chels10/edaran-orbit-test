<?php

namespace App\Http\Controllers;

use App\MultitenantAssignmentModel;
use Illuminate\Http\Request;

class MultitenantAssignmentModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MultitenantAssignmentModel  $multitenantAssignmentModel
     * @return \Illuminate\Http\Response
     */
    public function show(MultitenantAssignmentModel $multitenantAssignmentModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MultitenantAssignmentModel  $multitenantAssignmentModel
     * @return \Illuminate\Http\Response
     */
    public function edit(MultitenantAssignmentModel $multitenantAssignmentModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MultitenantAssignmentModel  $multitenantAssignmentModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MultitenantAssignmentModel $multitenantAssignmentModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MultitenantAssignmentModel  $multitenantAssignmentModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(MultitenantAssignmentModel $multitenantAssignmentModel)
    {
        //
    }
}
