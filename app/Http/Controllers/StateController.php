<?php

namespace App\Http\Controllers;

use App\StateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StateController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    public function retrieveall(){
        $result = StateModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = "To list all state for respective country";

        return json_encode($details);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id',
            'state_name',
            'country_id',

        */

        $validator = Validator::make($request->all(), [
            //'id' => 'required|integer',
            'state_name' => 'required|max:100',
            'country_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $state_name = $request->get('state_name');
        $country_id = $request->get('country_id');

        $model = new StateModel();
        $model->fill([
            'state_name' => $state_name,
            'country_id' => $country_id
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StateModel  $stateModel
     * @return \Illuminate\Http\Response
     */
    public function show(StateModel $stateModel , Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = StateModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StateModel  $stateModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StateModel $stateModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'state_name' => 'required|max:100',
            'country_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $state_name = $request->get('state_name');
        $country_id = $request->get('country_id');

        $model = StateModel::find($id);
        $model->fill([
            'id' => $id,
            'state_name' => $state_name,
            'country_id' => $country_id
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StateModel  $stateModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(StateModel $stateModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = StateModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
