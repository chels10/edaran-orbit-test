<?php

namespace App\Http\Controllers;

use App\EmploymentTypeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmploymentTypeController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /*
        Module : Core Employment Type
        Author : ShahKhairudin
        OWner  : Edaran Berhad
        Purpose : To retrieve employment type
    */

    public function retrieveall(){
        return EmploymentTypeModel::all();
        //return json_encode(['Response' => '200', 'Result' => $employmentTypes]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
            'id',
            'employment_type_name',
            'created_at',
            'updated_at'
        */

        $validator = Validator::make($request->all(), [
            'employment_type_name' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $employment_type_name = $request->get('employment_type_name');
        $model = new EmploymentTypeModel();
        $model->fill([
            'employment_type_name' => $employment_type_name,
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmploymentTypeModel  $employmentTypeModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, EmploymentTypeModel $employmentTypeModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = EmploymentTypeModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmploymentTypeModel  $employmentTypeModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmploymentTypeModel $employmentTypeModel)
    {
        /*
            'id',
            'employment_type_name',
            'created_at',
            'updated_at'
        */

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'employment_type_name' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $employment_type_name = $request->get('employment_type_name');
        $model = EmploymentTypeModel::find($id);
        $model->fill([
            'employment_type_name' => $employment_type_name,
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmploymentTypeModel  $employmentTypeModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, EmploymentTypeModel $employmentTypeModel)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = EmploymentTypeModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
