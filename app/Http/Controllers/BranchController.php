<?php

namespace App\Http\Controllers;

use App\BranchModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.verify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = BranchModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id', 'id_company', 'id_state', 'branch_type', 'branch_address', 'coordinate'

        */

        $validator = Validator::make($request->all(), [
            //'id' => 'required|integer',
            'id_company' => 'required|integer',
            'id_state' => 'required|integer',
            'branch_type' => 'required|max:100',
            'branch_address' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id_company = $request->get('id_company');
        $id_state = $request->get('id_state');
        $branch_type = $request->get('branch_type');
        $branch_address = $request->get('branch_address');
        $coordinate = $request->get('coordinate');

        $model = new BranchModel();
        $model->fill([
            'id_company' => $id_company,
            'id_state' => $id_state,
            'branch_type' => $branch_type,
            'branch_address' => $branch_address,
            'coordinate' => $coordinate,
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BranchModel  $branchModel
     * @return \Illuminate\Http\Response
     */
    public function show(BranchModel $branchModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = BranchModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BranchModel  $branchModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BranchModel $branchModel)
    {
        /*

            'id', 'id_company', 'id_state', 'branch_type', 'branch_address', 'coordinate'

        */

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'id_company' => 'required|integer',
            'id_state' => 'required|integer',
            'branch_type' => 'required|max:100',
            'branch_address' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $id_company = $request->get('id_company');
        $id_state = $request->get('id_state');
        $branch_type = $request->get('branch_type');
        $branch_address = $request->get('branch_address');
        $coordinate = $request->get('coordinate');

        $model = BranchModel::findorfail($id);
        $model->fill([
            'id_company' => $id_company,
            'id_state' => $id_state,
            'branch_type' => $branch_type,
            'branch_address' => $branch_address,
            'coordinate' => $coordinate,
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BranchModel  $branchModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(BranchModel $branchModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = BranchModel::findorfail($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
