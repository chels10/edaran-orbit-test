<?php

namespace App\Http\Controllers;

use App\ProfileModel;
use Illuminate\Http\Request;

class ProfileModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfileModel  $profileModel
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileModel $profileModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfileModel  $profileModel
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfileModel $profileModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfileModel  $profileModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfileModel $profileModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfileModel  $profileModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileModel $profileModel)
    {
        //
    }
}
