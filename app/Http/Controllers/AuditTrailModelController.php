<?php

namespace App\Http\Controllers;

use App\AuditTrailModel;
use Illuminate\Http\Request;

class AuditTrailModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuditTrailModel  $auditTrailModel
     * @return \Illuminate\Http\Response
     */
    public function show(AuditTrailModel $auditTrailModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuditTrailModel  $auditTrailModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AuditTrailModel $auditTrailModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuditTrailModel  $auditTrailModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuditTrailModel $auditTrailModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuditTrailModel  $auditTrailModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditTrailModel $auditTrailModel)
    {
        //
    }
}
