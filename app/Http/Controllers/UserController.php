<?php

namespace App\Http\Controllers;

use App\User;
use App\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{



    /* AUTHENTICATION SECTION */

    public function authenticate(Request $request){
        $token_expire_fancy = date("F j, Y, H:i", strtotime('+1 hour'));
        $token_expire = date("Y-m-d H:i:s", strtotime('+1 hour'));
        $token_request = date("Y-m-d H:i:s");
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 'invalid_credentials','code' => 100], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token','token_request','token_expire','token_expire_fancy'),200);
    }

    public function register(Request $request){

        $validator = Validator::make($request->all(), [
	        'name' => 'required|string|max:35',
	        'email' => 'required|string|email|max:25|unique:users',
	        'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
                $error_txt=$validator->errors();
                return response()->json(['status' => 'validation error', 'errors' => $error_txt], 400);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser(){
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
        }
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());

        }
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());

        }
        catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    /* SYSTEM API SECTION*/

    /*
        Module : User
        Author : ShahKhairudin
        OWner  : Edaran Berhad
        Purpose : To retrieve users
    */

    public function retrieveall(){
        $users = UserModel::all();

        foreach ($users as $object) {

            $object->dpermanent = $object->district_permanent->district_name;
            $object->spermanent = $object->state_permanent->state_name;

            $object->dcorrespondence = $object->district_correspondence;
            $object->scorrespondence = $object->state_correspondence;

        }

        return json_encode($users);
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        /*
            user_id
            staff_no
            full_name
            surf_name
            email
            password
            marital_status
            ic_new
            ic_old
            address_permanent
            postcode_permanent
            district_id_permanent
            state_id_permanent
            address_correspondence
            postcode_correspondence
            district_id_correspondence
            state_id_correspondence
            date_of_birth
            date_joined
            employment_type_id
            mobile_phone
            office_phone
            extension_no
            home_phone
            status
        */

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|max:100',
            'staff_no' => 'required|integer',
            'full_name' => 'required|max:100',
            'surf_name' => 'required|max:100',
            'email' => 'required|max:100',
            'password' => 'required|max:100',
            'marital_status' => 'required|max:10',
            'ic_new' => 'required|max:100',
            //'ic_old' => 'required|max:100',
            'address_permanent' => 'required|max:250',
            'postcode_permanent' => 'required|max:10',
            'district_id_permanent' => 'integer',
            'state_id_permanent' => 'required|integer',
            'address_correspondence' => 'required|max:250',
            'postcode_correspondence' => 'required|max:10',
            'district_id_correspondence' => 'required|integer',
            'state_id_correspondence' => 'required|integer',
            //'date_of_birth' => 'required',
            //'date_joined' => 'required',
            'employment_type_id' => 'required|integer',
            'mobile_phone' => 'required|max:50',
            'office_phone' => 'required|max:50',
            'extension_no' => 'required|integer',
            //'home_phone' => 'required|max:50',
            'status' => 'required|max:20'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $user_id = $request->get('user_id');
        $staff_no = $request->get('staff_no');
        $full_name = $request->get('full_name');
        $surf_name = $request->get('surf_name');
        $email = $request->get('email');
        $password = $request->get('password');
        $marital_status = $request->get('marital_status');
        $ic_new = $request->get('ic_new');
        $ic_old = $request->get('ic_old');
        $address_permanent = $request->get('address_permanent');
        $postcode_permanent = $request->get('postcode_permanent');
        $district_id_permanent = $request->get('district_id_permanent');
        $state_id_permanent = $request->get('state_id_permanent');
        $address_correspondence = $request->get('address_correspondence');
        $postcode_correspondence = $request->get('postcode_correspondence');
        $district_id_correspondence = $request->get('district_id_correspondence');
        $state_id_correspondence = $request->get('state_id_correspondence');
        $date_of_birth = $request->get('date_of_birth');
        $date_joined = $request->get('date_joined');
        $employment_type_id = $request->get('employment_type_id');
        $mobile_phone = $request->get('mobile_phone');
        $office_phone = $request->get('office_phone');
        $extension_no = $request->get('extension_no');
        $home_phone = $request->get('home_phone');
        $status = $request->get('status');

        $model = new UserModel();
        $model->fill([
            'user_id' => $user_id,
            'staff_no' => $staff_no,
            'full_name' => $full_name,
            'surf_name' => $surf_name,
            'email' => $email,
            'password' => $password,
            'marital_status' => $marital_status,
            'ic_new' => $ic_new,
            'ic_old' => $ic_old,
            'address_permanent' => $address_permanent,
            'postcode_permanent' => $postcode_permanent,
            'district_id_permanent' => $district_id_permanent,
            'state_id_permanent' => $state_id_permanent,
            'address_correspondence' => $address_correspondence,
            'postcode_correspondence' => $postcode_correspondence,
            'district_id_correspondence' => $district_id_correspondence,
            'state_id_correspondence' => $state_id_correspondence,
            'date_of_birth' => $date_of_birth,
            'date_joined' => $date_joined,
            'employment_type_id' => $employment_type_id,
            'mobile_phone' => $mobile_phone,
            'office_phone' => $office_phone,
            'extension_no' => $extension_no,
            'home_phone' => $home_phone,
            'status' => $status
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }

     /**
     * Display the specified resource.
     *
     * @param  \App\UserModel  $organizationModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, UserModel $userModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = UserModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserModel  $organizationModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, UserModel $userModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = UserModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserModel  $userModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserModel $userModel)
    {
        $validator = Validator::make($request->all(), [
            'id'=>'required|integer',
            'user_id' => 'required|max:100',
            'staff_no' => 'required|integer',
            'full_name' => 'required|max:100',
            'surf_name' => 'required|max:100',
            'email' => 'required|max:100',
            'password' => 'required|max:100',
            'marital_status' => 'required|max:10',
            //'ic_new' => 'required|max:100',
            //'ic_old' => 'required|max:100',
            'address_permanent' => 'required|max:250',
            'postcode_permanent' => 'required|max:10',
            'district_id_permanent' => 'required|integer',
            'state_id_permanent' => 'required|integer',
            'address_correspondence' => 'required|max:250',
            'postcode_correspondence' => 'required|max:10',
            'district_id_correspondence' => 'required|integer',
            'state_id_correspondence' => 'required|integer',
            'date_of_birth' => 'required',
            'date_joined' => 'required',
            'employment_type_id' => 'required|integer',
            'mobile_phone' => 'required|max:50',
            'office_phone' => 'required|max:50',
            'extension_no' => 'required|integer',
            //'home_phone' => 'required|max:50',
            'status' => 'required|max:20'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $user_id = $request->get('user_id');
        $staff_no = $request->get('staff_no');
        $full_name = $request->get('full_name');
        $surf_name = $request->get('surf_name');
        $email = $request->get('email');
        $password = $request->get('password');
        $marital_status = $request->get('marital_status');
        $ic_new = $request->get('ic_new');
        $ic_old = $request->get('ic_old');
        $address_permanent = $request->get('address_permanent');
        $postcode_permanent = $request->get('postcode_permanent');
        $district_id_permanent = $request->get('district_id_permanent');
        $state_id_permanent = $request->get('state_id_permanent');
        $address_correspondence = $request->get('address_correspondence');
        $postcode_correspondence = $request->get('postcode_correspondence');
        $district_id_correspondence = $request->get('district_id_correspondence');
        $state_id_correspondence = $request->get('state_id_correspondence');
        $date_of_birth = $request->get('date_of_birth');
        $date_joined = $request->get('date_joined');
        $employment_type_id = $request->get('employment_type_id');
        $mobile_phone = $request->get('mobile_phone');
        $office_phone = $request->get('office_phone');
        $extension_no = $request->get('extension_no');
        $home_phone = $request->get('home_phone');
        $status = $request->get('status');

        $model = UserModel::find($id);
        $model->fill([
            'user_id' => $user_id,
            'staff_no' => $staff_no,
            'full_name' => $full_name,
            'surf_name' => $surf_name,
            'email' => $email,
            'password' => $password,
            'marital_status' => $marital_status,
            'ic_new' => $ic_new,
            'ic_old' => $ic_old,
            'address_permanent' => $address_permanent,
            'postcode_permanent' => $postcode_permanent,
            'district_id_permanent' => $district_id_permanent,
            'state_id_permanent' => $state_id_permanent,
            'address_correspondence' => $address_correspondence,
            'postcode_correspondence' => $postcode_correspondence,
            'district_id_correspondence' => $district_id_correspondence,
            'state_id_correspondence' => $state_id_correspondence,
            'date_of_birth' => $date_of_birth,
            'date_joined' => $date_joined,
            'employment_type_id' => $employment_type_id,
            'mobile_phone' => $mobile_phone,
            'office_phone' => $office_phone,
            'extension_no' => $extension_no,
            'home_phone' => $home_phone,
            'status' => $status
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
