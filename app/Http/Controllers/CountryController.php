<?php

namespace App\Http\Controllers;

use App\CountryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CountryController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = CountryModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id', 'country_code', 'country_name', 'country_region'

        */

        $validator = Validator::make($request->all(), [
            //'id' => 'required|integer',
            'country_code' => 'required|max:20',
            'country_name' => 'required|max:250',
            'country_region' => 'required|max:250'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $country_code = $request->get('country_code');
        $country_name = $request->get('country_name');
        $country_region = $request->get('country_region');

        $model = new CountryModel();
        $model->fill([
            'country_code' => $country_code,
            'country_name' => $country_name,
            'country_region' => $country_region
        ]);

        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CountryModel  $countryModel
     * @return \Illuminate\Http\Response
     */
    public function show(CountryModel $countryModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = CountryModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CountryModel  $countryModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CountryModel $countryModel)
    {
        /*

            'id', 'country_code', 'country_name', 'country_region'

        */

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'country_code' => 'required|max:20',
            'country_name' => 'required|max:250',
            'country_region' => 'required|max:250'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $country_code = $request->get('country_code');
        $country_name = $request->get('country_name');
        $country_region = $request->get('country_region');

        $model = CountryModel::findorfail($id);
        $model->fill([
            'country_code' => $country_code,
            'country_name' => $country_name,
            'country_region' => $country_region
        ]);

        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CountryModel  $countryModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(CountryModel $countryModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = CountryModel::findorfail($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
