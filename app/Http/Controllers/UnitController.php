<?php

namespace App\Http\Controllers;

use App\UnitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = UnitModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id',
            'id_section',
            'unit_name',
            'id_head_of_unit',

        */

        $validator = Validator::make($request->all(), [
            'id_section' => 'required|integer',
            'unit_name' => 'required|max:100',
            'id_head_of_unit' => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id_section = $request->get('id_section');
        $unit_name = $request->get('unit_name');
        $id_head_of_unit = $request->get('id_head_of_unit');

        $model = new UnitModel();
        $model->fill([
            'id_section' => $id_section,
            'unit_name' => $unit_name,
            'id_head_of_unit' => $id_head_of_unit
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnitModel  $unitModel
     * @return \Illuminate\Http\Response
     */
    public function show(UnitModel $unitModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = UnitModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnitModel  $unitModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UnitModel $unitModel)
    {
        /*

            'id',
            'id_section',
            'unit_name',
            'id_head_of_unit',

        */

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'id_section' => 'required|integer',
            'unit_name' => 'required|max:100',
            'id_head_of_unit' => 'required|max:50'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $id_section = $request->get('id_section');
        $unit_name = $request->get('unit_name');
        $id_head_of_unit = $request->get('id_head_of_unit');

        $model = UnitModel::find($id);
        $model->fill([
            'id_section' => $id_section,
            'unit_name' => $unit_name,
            'id_head_of_unit' => $id_head_of_unit
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnitModel  $unitModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(UnitModel $unitModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = UnitModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
