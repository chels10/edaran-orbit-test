<?php

namespace App\Http\Controllers;

use App\DistrictModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DistrictController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /* Retrieve Districts */
    public function retrieveall(){

        $result = DistrictModel::with('state')->get();
        // foreach ($result as $object) {
        //     $object->statename = $object->state->state_name;
        // }
        return json_encode(['Response' => '200', 'Result' => $result]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
            'id',
            'district_name',
            'state_id',
        */

        $validator = Validator::make($request->all(), [
            'district_name' => 'required|max:100',
            'state_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $district_name = $request->get('district_name');
        $state_id = $request->get('state_id');

        $model = new DistrictModel();
        $model->fill([
            'district_name' => $district_name,
            'state_id' => $state_id
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DistrictModel  $districtModel
     * @return \Illuminate\Http\Response
     */
    public function show(DistrictModel $districtModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');

        $result = DistrictModel::with('state')->whereIn('id', array($id))->get();
        foreach ($result as $object) {
            $object->statename = $object->state->state_name;
        }

        return json_encode($result);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DistrictModel  $districtModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DistrictModel $districtModel)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'district_name' => 'required|max:100',
            'state_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $district_name = $request->get('district_name');
        $state_id = $request->get('state_id');

        $model = DistrictModel::find($id);
        $model->fill([
            'district_name' => $district_name,
            'state_id' => $state_id
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DistrictMopdel  $districtModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(DistrictModel $districtModel , Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = DistrictModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
