<?php

namespace App\Http\Controllers;
use App\TransportModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransportController extends Controller
{

    public function index()
    {
      $result = TransportModel::all();
      return json_encode(['Response' => '200', 'Result' => $result]);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'transport_type' => 'required|max:100',
            'claim' => 'required|numeric|min:0|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }
        $model = new TransportModel();
        $model->fill([
            'transport_type' => $request->get('transport_type'),
            'claim' => $request->get('claim'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function show(Request $request,$id)
    {
        $result = TransportModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'transport_type' => 'required|max:100',
            'claim' => 'required|numeric|min:0|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }

        $id = $request->get('id');

        $model = TransportModel::findOrFail($id);
        $model->fill([
          'transport_type' => $request->get('transport_type'),
          'claim' => $request->get('claim'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function destroy(Request $request, $id)
    {

        $result = TransportModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
