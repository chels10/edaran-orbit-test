<?php

namespace App\Http\Controllers;

use App\SectionModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SectionController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = DepartmentModel::all();
        return json_encode(['Response' => '200', 'Result' => $result]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*

            'id',
            'id_deparment',
            'section_name',
            'id_head_of_department',

        */

        $validator = Validator::make($request->all(), [
            'id_department' => 'required|integer',
            'section_name' => 'required|max:100',
            'id_head_of_department' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id_department = $request->get('id_department');
        $section_name = $request->get('section_name');
        $id_head_of_department = $request->get('id_head_of_department');

        $model = new SectionModel();
        $model->fill([
            'id_department' => $id_department,
            'section_name' => $section_name,
            'id_head_of_department' => $id_head_of_department
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SectionModel  $sectionModel
     * @return \Illuminate\Http\Response
     */
    public function show(SectionModel $sectionModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = SectionModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SectionModel  $sectionModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SectionModel $sectionModel)
    {
        /*

            'id',
            'id_deparment',
            'section_name',
            'id_head_of_department',

        */

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'id_department' => 'required|integer',
            'section_name' => 'required|max:100',
            'id_head_of_department' => 'required|max:100'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $id = $request->get('id');
        $id_department = $request->get('id_department');
        $section_name = $request->get('section_name');
        $id_head_of_department = $request->get('id_head_of_department');

        $model = SectionModel::find($id);
        $model->fill([
            'id_department' => $id_department,
            'section_name' => $section_name,
            'id_head_of_department' => $id_head_of_department
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SectionModel  $sectionModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(SectionModel $sectionModel, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('id');
        $result = SectionModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
