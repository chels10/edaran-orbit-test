<?php

namespace App\Http\Controllers;
use App\UserChildModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserChildController extends Controller
{

    public function index()
    {
      $result = UserChildModel::all();
      return json_encode(['Response' => '200', 'Result' => $result]);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|max:100',
            'child_name' => 'required|max:100',
            'child_ic' => 'required|max:100',
            'child_martial' => 'required|max:100',
            'child_edu' => 'required|max:100',
            'child_level' => 'required|max:100',
            'child_insti' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode([
              'Response' => '400',
              'error' => $validator->messages()->all()]);
        }
        $model = new UserChildModel();
        $model->fill([
            'user_id' => $request->get('user_id'),
            'child_name' => $request->get('child_name'),
            'child_ic' => $request->get('child_ic'),
            'child_martial' => $request->get('child_martial'),
            'child_edu' => $request->get('child_edu'),
            'child_level' => $request->get('child_level'),
            'child_insti' => $request->get('child_insti'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function show(Request $request,$id)
    {
        $result = UserChildModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'user_id' => 'required|max:100',
            'child_name' => 'required|max:100',
            'child_ic' => 'required|max:100',
            'child_martial' => 'required|max:100',
            'child_edu' => 'required|max:100',
            'child_level' => 'required|max:100',
            'child_insti' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }

        $id = $request->get('id');

        $model = UserChildModel::findOrFail($id);
        $model->fill([
          'user_id' => $request->get('user_id'),
          'child_name' => $request->get('child_name'),
          'child_ic' => $request->get('child_ic'),
          'child_martial' => $request->get('child_martial'),
          'child_edu' => $request->get('child_edu'),
          'child_level' => $request->get('child_level'),
          'child_insti' => $request->get('child_insti'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function destroy(Request $request, $id)
    {

        $result = UserChildModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
