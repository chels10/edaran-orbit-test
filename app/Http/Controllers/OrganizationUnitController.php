<?php

namespace App\Http\Controllers;

use App\OrganizationUnitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class OrganizationUnitController extends Controller
{

    public function __construct(){
        #$this->middleware('jwt.verify');
    }

    /*
        Purpose : To retrieve all - TEST Purpose
    */
    public function retrieveall(){
        $result = OrganizationUnitModel::all();
        return json_encode($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
            'orgunit_code',
            'orgunit_name',
            'address',
            'postal_code',
            'district_id',
            'state_id',
            'country_id',
            'office_phone',
            'mobile_phone',
            'fax',
            'email',
            'remark'
        */

        $validator = Validator::make($request->all(), [
            'orgunit_name' => 'required',
            'address' => 'required',
            'postal_code' => 'required',
            'district_id' => 'required|integer',
            'state_id' => 'required|integer',
            //'country_id' => 'required',
            'office_phone' => 'required',
            'mobile_phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $orgunit_name = $request->get('orgunit_name');
        $address = $request->get('address');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $country_id = $request->get('country_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = new OrganizationUnitModel();
        $model->fill([
            'orgunit_name' => $orgunit_name,
            'address' => $address,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'country_id' => $country_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizationUnitModel  $organizationUnitModel
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, OrganizationUnitModel $organizationUnitModel)
    {
        $validator = Validator::make($request->all(), [
            'orgunit_code' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('orgunit_code');
        $result = OrganizationUnitModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationUnitModel  $organizationUnitModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizationUnitModel $organizationUnitModel)
    {

        $validator = Validator::make($request->all(), [
            'orgunit_code' => 'required',
            'orgunit_name' => 'required',
            'address' => 'required',
            'postal_code' => 'required',
            'district_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'office_phone' => 'required',
            'mobile_phone' => 'required',
            'fax' => 'required',
            'email' => 'required',
            'remark' => 'required'
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $orgunit_code = $request->get('orgunit_code');
        $orgunit_name = $request->get('orgunit_name');
        $postal_code = $request->get('postal_code');
        $district_id = $request->get('district_id');
        $state_id = $request->get('state_id');
        $country_id = $request->get('country_id');
        $office_phone = $request->get('office_phone');
        $mobile_phone = $request->get('mobile_phone');
        $fax = $request->get('fax');
        $email = $request->get('email');
        $remark = $request->get('remark');

        $model = OrganizationUnitModel::find($orgunit_code);
        $model->fill([
            'orgunit_name' => $orgunit_name,
            'postal_code' => $postal_code,
            'district_id' => $district_id,
            'state_id' => $state_id,
            'country_id' => $country_id,
            'office_phone' => $office_phone,
            'mobile_phone' => $mobile_phone,
            'fax' => $fax,
            'email' => $email,
            'remark' => $remark
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationUnitModel  $organizationUnitModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, OrganizationUnitModel $organizationUnitModel)
    {
        $validator = Validator::make($request->all(), [
            'orgunit_code' => 'required',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }
        $id = $request->get('orgunit_code');
        $result = OrganizationUnitModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }
}
