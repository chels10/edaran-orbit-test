<?php

namespace App\Http\Controllers;
use App\UserKinModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Rules\fkstate;

class UserKinController extends Controller
{

    public function index()
    {
      $result = UserKinModel::all();
      return json_encode(['Response' => '200', 'Result' => $result]);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|max:100',
            'kin_name' => 'required|max:100',
            'kin_address' => 'required|max:100',
            'kin_postcode' => 'required|max:100',
            'kin_district' => 'required|max:100',
            'kin_state' => ['required', 'string', new fkstate],
            'kin_contact' => 'required|max:100',
            'kin_relation' => 'required|max:100',
            'kin_district' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }
        $model = new UserKinModel();
        $model->fill([
            'user_id' => $request->get('user_id'),
            'kin_name' => $request->get('kin_name'),
            'kin_address' => $request->get('kin_address'),
            'kin_postcode' => $request->get('kin_postcode'),
            'kin_district' => $request->get('kin_district'),
            'kin_state' => $request->get('kin_state'),
            'kin_contact' => $request->get('kin_contact'),
            'kin_relation' => $request->get('kin_relation'),
            'kin_district' => $request->get('kin_district'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function show(Request $request,$id)
    {
        $result = UserKinModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'user_id' => 'required|max:100',
            'kin_name' => 'required|max:100',
            'kin_address' => 'required|max:100',
            'kin_postcode' => 'required|max:100',
            'kin_district' => 'required|max:100',
            'kin_state' => ['required', 'string', new fkstate],
            'kin_contact' => 'required|max:100',
            'kin_relation' => 'required|max:100',
            'kin_district' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }

        $id = $request->get('id');

        $model = UserKinModel::findOrFail($id);
        $model->fill([
            'user_id' => $request->get('user_id'),
            'kin_name' => $request->get('kin_name'),
            'kin_address' => $request->get('kin_address'),
            'kin_postcode' => $request->get('kin_postcode'),
            'kin_district' => $request->get('kin_district'),
            'kin_state' => $request->get('kin_state'),
            'kin_contact' => $request->get('kin_contact'),
            'kin_relation' => $request->get('kin_relation'),
            'kin_district' => $request->get('kin_district'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function destroy(Request $request, $id)
    {

        $result = UserKinModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
