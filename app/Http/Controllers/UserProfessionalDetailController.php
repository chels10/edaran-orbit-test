<?php

namespace App\Http\Controllers;
use App\UserProfessionalDetailModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserProfessionalDetailController extends Controller
{

    public function index()
    {
      $result = UserProfessionalDetailModel::all();
      return json_encode(['Response' => '200', 'Result' => $result]);
    }


    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|max:100',
            'staff_no' => 'required|max:100',
            'job_grade' => 'required|max:50',
            'designation' => 'required|max:150',
            'unit_base' => 'required|max:100',
            'branch_id' => 'required|numeric|min:1|max:100',
            'department_id' => 'required|numeric|min:1|max:100',
            'unit_id' => 'required|numeric|min:1|max:100',
            'company_code_id' => 'required|numeric|min:1|max:100',
            'organization_unit_code_id' => 'required|numeric|min:1|max:100',
            'date_joined' => 'required|date',
            'date_exited' => 'required|date',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }
        $model = new UserProfessionalDetailModel();
        $model->fill([
            'user_id' => $request->get('user_id'),
            'staff_no' => $request->get('staff_no'),
            'job_grade' => $request->get('job_grade'),
            'designation' => $request->get('designation'),
            'unit_base' => $request->get('unit_base'),
            'branch_id' => $request->get('branch_id'),
            'department_id' => $request->get('department_id'),
            'unit_id' => $request->get('unit_id'),
            'company_code_id' => $request->get('company_code_id'),
            'organization_unit_code_id' => $request->get('organization_unit_code_id'),
            'date_joined' => $request->get('date_joined'),
            'date_exited' => $request->get('date_exited'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function show(Request $request,$id)
    {
        $result = UserProfessionalDetailModel::find($id);
        if ($result) {
            return json_encode(['Response' => '200', 'Result' => $result]);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
            'user_id' => 'required|max:100',
            'staff_no' => 'required|max:100',
            'job_grade' => 'required|max:50',
            'designation' => 'required|max:150',
            'unit_base' => 'required|max:100',
            'branch_id' => 'required|numeric|min:1|max:100',
            'department_id' => 'required|numeric|min:1|max:100',
            'unit_id' => 'required|numeric|min:1|max:100',
            'company_code_id' => 'required|numeric|min:1|max:100',
            'organization_unit_code_id' => 'required|numeric|min:1|max:100',
            'date_joined' => 'required|date',
            'date_exited' => 'required|date',
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->all()]);
        }

        $id = $request->get('id');

        $model = UserProfessionalDetailModel::findOrFail($id);
        $model->fill([
          'user_id' => $request->get('user_id'),
          'staff_no' => $request->get('staff_no'),
          'job_grade' => $request->get('job_grade'),
          'designation' => $request->get('designation'),
          'unit_base' => $request->get('unit_base'),
          'branch_id' => $request->get('branch_id'),
          'department_id' => $request->get('department_id'),
          'unit_id' => $request->get('unit_id'),
          'company_code_id' => $request->get('company_code_id'),
          'organization_unit_code_id' => $request->get('organization_unit_code_id'),
          'date_joined' => $request->get('date_joined'),
          'date_exited' => $request->get('date_exited'),
        ]);
        $result = $model->save();

        if ($result) {
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }
    }


    public function destroy(Request $request, $id)
    {

        $result = UserProfessionalDetailModel::find($id);
        if ($result) {
            $result->delete();
            return json_encode(['Response' => '200']);
        } else {
            return json_encode(['Response' => '400']);
        }

    }
}
