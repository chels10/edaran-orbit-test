<?php

namespace App\Http\Middleware;

use App\AuditTrailModel;
use Closure;
use Illuminate\Support\Facades\Validator;

class MW_AuditTrail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $validator = Validator::make($request->all(), [
            'module' => 'required',
            
        ]);

        if ($validator->fails()) {
            return json_encode(['error' => $validator->messages()->first()]);
        }

        $module = $request->route('module');
        $user_id = $request->route('user_id');
        $process = $request->route('process');
        $value = $request->route('value');

        $model = new AuditTrailModel();
        $model->fill([
                'module' => $module,
                'user_id' => $user_id,
                'process' => $process,
                'value' => $value
            ]);
        $model->save();
        return $next($request);
    }
}
