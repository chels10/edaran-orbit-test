<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SectionModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_section';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'id', 'id_department', 'section_name', 'id_head_of_department'
    ];

    protected $auditInclude = [
        'id', 'id_department', 'section_name', 'id_head_of_department'
    ];
}
