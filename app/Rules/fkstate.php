<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\StateModel;

class fkstate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = StateModel::find($value);
        if ($user instanceof  StateModel) {
          return True;
        }
        return False;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'State not found';
    }
}
