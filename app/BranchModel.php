<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class BranchModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_branch';
    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = [
    	'id', 'id_company', 'id_state', 'branch_type', 'branch_address', 'coordinate'
    ];


    protected $auditInclude = [
        'id', 'id_company', 'id_state', 'branch_type', 'branch_address', 'coordinate'
    ];

}
