<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CountryModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_country';
    protected $primaryKey = 'id';

    // public $incrementing = false;

    protected $fillable = [
    	'id', 'country_code', 'country_name', 'country_region'
    ];


    protected $auditInclude = [
        'id', 'country_code', 'country_name', 'country_region'
    ];
}
