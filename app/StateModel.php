<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class StateModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_state';
    protected $primaryKey = 'id';

    #public $incrementing = false;

    protected $fillable = [
    	'id', 'state_name', 'country_id'
    ];


    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',  'state_name', 'country_id'
    ];

    public function district(){
        return $this->hasMany('App\DistrictModel', 'state_id');
    }

}
