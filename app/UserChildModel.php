<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class UserChildModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'user_child';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'user_id','child_name','child_ic','child_martial','child_edu','child_level','child_insti'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'child_name',
        'child_ic',
        'child_martial',
        'child_edu',
        'child_level',
        'child_insti'
    ];
}
