<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'users';
    protected $primaryKey = 'id';

    //protected $appends = ['dpermanent', 'spermanent', 'dcorrespondence', 'scorrespondence'];

    protected $appends = ['dpermanent', 'spermanent'];

    protected $fillable = [
            'id',
    	    'user_id',
            'staff_no',
            'full_name',
            'surf_name',
            'email',
            'password',
            'marital_status',
            'ic_new',
            'ic_old',
            'address_permanent',
            'postcode_permanent',
            'district_id_permanent',
            'state_id_permanent',
            'address_correspondence',
            'postcode_correspondence',
            'district_id_correspondence',
            'state_id_correspondence',
            'date_of_birth',
            'date_joined',
            'employment_type_id',
            'mobile_phone',
            'office_phone',
            'extension_no',
            'home_phone',
            'status'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'user_id',
        'staff_no',
        'full_name',
        'surf_name',
        'email',
        'password',
        'marital_status',
        'ic_new',
        'ic_old',
        'address_permanent',
        'postcode_permanent',
        'district_id_permanent',
        'state_id_permanent',
        'address_correspondence',
        'postcode_correspondence',
        'district_id_correspondence',
        'state_id_correspondence',
        'date_of_birth',
        'date_joined',
        'employment_type_id',
        'mobile_phone',
        'office_phone',
        'extension_no',
        'home_phone',
        'status',
    ];

    /* MUTATOR */

    public function setDpermanentAttribute($district){
        $this->attributes['dpermanent'] = $district;
    }

    public function getDpermanentAttribute(){
        return $this->attributes['dpermanent'];
    }

    public function setSpermanentAttribute($state){
        $this->attributes['spermanent'] = $state;
    }

    public function getSpermanentAttribute(){
        return $this->attributes['spermanent'];
    }

    public function setDisAttribute($districtcorr){
        $this->attributes['districtcorrespondence'] = $districtcorr;
    }

    public function getDisAttribute(){
        return $this->attributes['districtcorrespondence'];
    }

    public function setSttAttribute($state_correspondence){
        $this->attributes['statecorrespondence'] = $state_correspondence;
    }

    public function getSttAttribute(){
        return $this->attributes['statecorrespondence'];
    }

    /* END OF MUTATOR */

    public function district_permanent(){
        return $this->belongsTo('App\DistrictModel', 'district_id_permanent');
    }

    public function district_correspondence(){
        return $this->belongsTo('App\DistrictModel', 'district_id_correspondence');
    }

    public function state_permanent(){
        return $this->belongsTo('App\StateModel', 'state_id_permanent');
    }

    public function state_correspondence(){
        return $this->belongsTo('App\StateModel', 'state_id_correspondence');
    }

}
