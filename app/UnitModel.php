<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UnitModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_unit';
    protected $primaryKey = 'id';

    protected $fillable = [
    	'id', 'id_section', 'unit_name', 'id_head_of_unit'
    ];

    protected $auditInclude = [
        'id', 'id_section', 'unit_name', 'id_head_of_unit'
    ];
}
