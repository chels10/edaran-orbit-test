<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class TransportModel extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_transport';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
    	'transport_type','claim'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',
        'transport_type',
        'claim',
    ];
}
