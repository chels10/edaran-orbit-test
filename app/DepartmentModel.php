<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class DepartmentModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_department';
    protected $primaryKey = 'id';

    #public $incrementing = false;

    protected $fillable = [
    	'id', 'dept_code', 'dept_name'
    ];


    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'id',  'dept_code', 'dept_name'
    ];

}
