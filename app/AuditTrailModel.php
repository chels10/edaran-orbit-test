<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrailModel extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'core_audit_trail';
    protected $primaryKey = 'id';

    public $incrementing = false;

    protected $fillable = [
    	'module', 'user_id', 'process','value'
    ];

    public function user(){
        return $this->belongsTo('App\UserModel', 'user_id', 'user_id');
    }
}
