<?php

use App\CompanyCodeModel;
use Illuminate\Http\Request;
use App\Http\Middleware;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Below are compulsary parameters to be sent for API calls (for audit trail purposes) :
|   1) authentication token
|   2) module
|   3) user_id
|   4) process
|   5) value
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

/*
    MODULE : Core District
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : In Progress
*/
Route::get('district/retrieveall', 'DistrictController@retrieveall')->name('Retreive districts');
Route::get('district/find', 'DistrictController@show')->name('find specific district details');
Route::put('district/put', 'DistrictController@store')->name('add district');
Route::delete('district/destroy', 'DistrictController@destroy')->name('delete district');
Route::post('district/update', 'DistrictController@update')->name('update district details');

/*
    MODULE : Company Code
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/
Route::get('companycode/retrieveall', 'CompanyCodeController@retrieveall')->name('Retreive companies');
Route::get('companycode/find', 'CompanyCodeController@show')->name('find specific company code details');
Route::put('companycode/put', 'CompanyCodeController@store')->name('add company code');
Route::delete('companycode/destroy', 'CompanyCodeController@destroy')->name('delete company code');
Route::post('companycode/update', 'CompanyCodeController@update')->name('update company code details');

/*
    MODULE : Organization Unit
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/
Route::get('organizationunit/retrieveall', 'OrganizationUnitController@retrieveall')->name('Retrieve Organization Units');
Route::get('organizationunit/find', 'OrganizationUnitController@show')->name('find specific organization unit details');
Route::put('organizationunit/put', 'OrganizationUnitController@store')->name('add organization unit');
Route::delete('organizationunit/destroy', 'OrganizationUnitController@destroy')->name('delete organization unit');
Route::post('organizationunit/update', 'OrganizationUnitController@update')->name('update organization unit details');

/*
    MODULE : Organization
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/
Route::get('organization/retrieveall', 'OrganizationController@retrieveall')->name('Retrieve Organizations');
Route::get('organization/find', 'OrganizationController@show')->name('find specific organization details');
Route::put('organization/put', 'OrganizationController@store')->name('add organization');
Route::delete('organization/destroy', 'OrganizationController@destroy')->name('delete organization');
Route::post('organization/update', 'OrganizationController@update')->name('update organization details');

/*
    MODULE : User
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/
Route::get('user/retrieveall', 'UserController@retrieveall')->name('Retreive users');
Route::get('user/find', 'UserController@show')->name('find specific user details');
Route::put('user/put', 'UserController@store')->name('add user');
Route::delete('user/destroy', 'UserController@destroy')->name('delete user');
Route::post('user/update', 'UserController@update')->name('update user details');

/* SPRINT 2 */

/*
    MODULE : Core Employment Type
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : completed
*/
Route::get('employmenttype/retrieveall', 'EmploymentTypeController@retrieveall')->name('Retreive employment type');
Route::get('employmenttype/find', 'EmploymentTypeController@show')->name('find specific employment type details');
Route::put('employmenttype/put', 'EmploymentTypeController@store')->name('add employment type');
Route::delete('employmenttype/destroy', 'EmploymentTypeController@destroy')->name('delete employment type');
Route::post('employmenttype/update', 'EmploymentTypeController@update')->name('update employment type details');

/*
    MODULE : Core State
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : in progress
*/
Route::get('state/retrieveall', 'StateController@retrieveall')->name('Retreive state');
Route::get('state/find', 'StateController@show')->name('find specific state details');
Route::put('state/put', 'StateController@store')->name('add state');
Route::delete('state/destroy', 'StateController@destroy')->name('delete state');
Route::post('state/update', 'StateController@update')->name('update state details');


/*
    MODULE : User Kin
    AUTHOR : Eddy Irwan
    OWNER  : Edaran Berhad
*/
Route::get('userkin/retrieveall', 'UserKinController@index')->name('userkin_retrieve');
Route::get('userkin/{id}/find', 'UserKinController@show')->name('userkin_show');
Route::put('userkin/put', 'UserKinController@store')->name('userkin_store');
Route::delete('userkin/{id}/destroy', 'UserKinController@destroy')->name('userkin_destroy');
Route::post('userkin/update', 'UserKinController@update')->name('userkin_update');

/*
    MODULE : User Child
    AUTHOR : Eddy Irwan
    OWNER  : Edaran Berhad
*/
Route::get('userchild/retrieveall', 'UserChildController@index')->name('userchild_retrieve');
Route::get('userchild/{id}/find', 'UserChildController@show')->name('userchild_show');
Route::put('userchild/put', 'UserChildController@store')->name('userchild_store');
Route::delete('userchild/{id}/destroy', 'UserChildController@destroy')->name('userchild_destroy');
Route::post('userchild/update', 'UserChildController@update')->name('userchild_update');


/*
    MODULE : Transport
    AUTHOR : Eddy Irwan
    OWNER  : Edaran Berhad
*/
Route::get('transport/retrieveall', 'TransportController@index')->name('transport_retrieve');
Route::get('transport/{id}/find', 'TransportController@show')->name('transport_show');
Route::put('transport/put', 'TransportController@store')->name('transport_store');
Route::delete('transport/{id}/destroy', 'TransportController@destroy')->name('transport_destroy');
Route::post('transport/update', 'TransportController@update')->name('transport_update');

/*
    MODULE : User Professional Detail
    AUTHOR : Eddy Irwan
    OWNER  : Edaran Berhad
*/
Route::get('userprofessionaldetail/retrieveall', 'UserProfessionalDetailController@index')->name('userProfessionalDetail_retrieve');
Route::get('userprofessionaldetail/{id}/find', 'UserProfessionalDetailController@show')->name('userProfessionalDetail_show');
Route::put('userprofessionaldetail/put', 'UserProfessionalDetailController@store')->name('userProfessionalDetail_store');
Route::delete('userprofessionaldetail/{id}/destroy', 'UserProfessionalDetailController@destroy')->name('userProfessionalDetail_destroy');
Route::post('userprofessionaldetail/update', 'UserProfessionalDetailController@update')->name('userProfessionalDetail_update');

/*
    MODULE : Department
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/

Route::get('department/index', 'DepartmentController@index')->name('Retrieve departments');
Route::get('department/find', 'DepartmentController@show')->name('find specific department details');
Route::put('department/put', 'DepartmentController@store')->name('add department');
Route::delete('department/destroy', 'DepartmentController@destroy')->name('delete department');
Route::post('department/update', 'DepartmentController@update')->name('update department details');

/*
    MODULE : Country
    AUTHOR : ShahKhairudin
    OWNER  : Edaran Berhad
    STATUS : Completed
*/

Route::get('country/index', 'CountryController@index')->name('Retrieve countries');
Route::get('country/find', 'CountryController@show')->name('find specific country details');
Route::put('country/put', 'CountryController@store')->name('add country');
Route::delete('country/destroy', 'CountryController@destroy')->name('delete country');
Route::post('country/update', 'CountryController@update')->name('update country details');
