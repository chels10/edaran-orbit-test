<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timesheet', function (Blueprint $table) {
            $table->increments('timeindex');
            $table->string('UserId', 100);
            $table->string('projectCode', 10);
            $table->string('activity', 100);
            $table->string('location', 100);
            $table->string('description', 300);
            $table->dateTime('timeStarted');
            $table->dateTime('timeEnded');
            $table->time('hours');
            $table->date('dateLog');
            $table->string('monthLog', 20);
            $table->integer('yearLog');
            $table->decimal('amount');
            $table->timestamps();

            $table->foreign('projectCode')->references('PrjctCode')->on('p_project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timesheet');
    }
}
