<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('BugID');
            $table->integer('user_id')->unsigned();
            $table->string('platform', 20);
            $table->string('category', 100);
            $table->string('title', 250);
            $table->string('description', 3000);
            $table->string('specification', 1000);
            $table->dateTime('timeSubmitted');
            $table->string('reviewedby', 10);
            $table->dateTime('timeUpdated');
            $table->tinyInteger('status');
            $table->tinyInteger('priority');
            $table->tinyInteger('timeline');
            $table->tinyInteger('archive');

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
