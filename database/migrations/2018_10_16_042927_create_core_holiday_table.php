<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_holiday', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('holiday_date');
            $table->string('holiday_year',5);
            $table->string('holiday_name');
            $table->float('holiday_state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_holiday');
    }
}
