<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_profile', function (Blueprint $table) {
            $table->string('profile_code')->unique();
            $table->string('profile_description', 250);
            $table->string('sub_module', 100);
            $table->integer('company_code')->unsigned();
            $table->date('created_at');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->date('updated_at');

            $table->index('created_by');
            $table->index('updated_by');
            $table->index('company_code');
            $table->index('profile_code');

            $table->foreign('company_code')->references('company_code')->on('y_companycode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_profile');
    }
}
