<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_country', function (Blueprint $table) {
          $table->integer('id');
          $table->string('country_code', 20);
          $table->string('country_name', 250);
          $table->string('country_region', 250);
          $table->timestamps();
          $table->index('country_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_country');
    }
}
