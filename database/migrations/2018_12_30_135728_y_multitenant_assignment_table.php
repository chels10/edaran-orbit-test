<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YMultitenantAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_multitenant_assignment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_code')->unsigned();
            $table->integer('orgunit_code')->unsigned();
            $table->integer('organization_code')->unsigned();
            $table->date('valid_from');
            $table->date('valid_to');
            $table->date('created_at');
            $table->string('created_by');
            $table->date('updated_at');
            $table->string('updated_by');

            $table->index('company_code');
            $table->index('orgunit_code');
            $table->index('organization_code');
            $table->index('created_by');
            $table->index('updated_by');

            $table->foreign('company_code')->references('company_code')->on('y_companycode');
            $table->foreign('organization_code')->references('id')->on('y_organization');
            $table->foreign('orgunit_code')->references('orgunit_code')->on('y_organizationunit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_multitenant_assignment');
    }
}
