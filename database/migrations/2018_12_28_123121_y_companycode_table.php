<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YCompanycodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_companycode', function (Blueprint $table) {
            $table->increments('company_code');
            $table->string('company_name', 100);
            $table->string('address', 250);
            $table->string('postal_code', 10);
            $table->integer('district_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('country_id');
            $table->string('office_phone', 50);
            $table->string('mobile_phone', 50);
            $table->string('fax', 50);
            $table->string('email', 150);
            $table->mediumText('remark');
            $table->timestamps();

            $table->index('district_id');
            $table->index('state_id');
            $table->index('country_id');
            $table->index('email');
            $table->index('mobile_phone');
            $table->index('company_name');

            $table->foreign('district_id')->references('id')->on('core_district');
            $table->foreign('state_id')->references('id')->on('core_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_companycode');
    }
}
