<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YOrganizationunitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_organizationunit', function (Blueprint $table) {
            $table->increments('orgunit_code');
            $table->string('orgunit_name', 50);
            $table->string('address', 250);
            $table->string('postal_code', 50);
            $table->integer('district_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->string('country_id', 20);
            $table->string('office_phone', 50);
            $table->string('mobile_phone', 50);
            $table->string('fax', 50);
            $table->string('email', 150);
            $table->mediumText('remark');
            $table->timestamps();

            $table->index('district_id');
            $table->index('state_id');
            $table->index('country_id');
            $table->index('email');
            $table->index('mobile_phone');
            $table->index('orgunit_name');

            $table->foreign('district_id')->references('id')->on('core_district');
            $table->foreign('state_id')->references('id')->on('core_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_organizationunit');
    }
}
