<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePProjectLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_project_location', function (Blueprint $table) {
            $table->increments('ProjectAreaID');
            $table->string('PrjctCode', 100);
            $table->string('DistrictID', 100);
            $table->string('CreateID', 100);
            $table->date('CreateDate');
            $table->string('UpdateID', 30);
            $table->date('UpdateDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_project_location');
    }
}
