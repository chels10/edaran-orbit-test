<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_project', function (Blueprint $table) {
            $table->string('PrjctCode')->unique();
            $table->string('ProjectName', 100);
            $table->text('ProjectDesc');
            $table->string('PrjctMgrID', 100);
            $table->string('AccManagerID', 100);
            $table->integer('CustomerID');
            $table->string('CreateID');
            $table->date('CreateDate');
            $table->string('UpdateID', 100);
            $table->date('UpdateDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_project');
    }
}
