<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDistrictTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_district', function (Blueprint $table) {
            $table->increments('id');
            $table->string('district_name', 100);
            $table->integer('state_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('core_district', function (Blueprint $table) {
            $table->foreign('state_id')
                ->references('id')
                ->on('core_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_district');
    }
}
