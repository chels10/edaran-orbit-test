<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YUserprofile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_userprofile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('UserId', 100);
            $table->string('role_code', 100);
            $table->date('valid_from');
            $table->date('valid_to');
            $table->timestamps();

            $table->foreign('role_code')->references('role_code')->on('y_role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_userprofile');
    }
}
