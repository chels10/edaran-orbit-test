<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class YRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('y_role', function (Blueprint $table) {
            $table->string('role_code', 100)->unique();
            $table->string('role_name', 100);
            $table->mediumText('role_description');
            $table->string('profile_code', 100);
            $table->index('role_code');
            $table->string('created_by', 100);
            $table->string('updated_by', 100);
            $table->index('profile_code');
            $table->index('created_by');
            $table->index('updated_by');
            $table->timestamps();

            $table->foreign('profile_code')->references('profile_code')->on('y_profile');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('y_role');
    }
}
