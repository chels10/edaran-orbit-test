<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id', 100);
            $table->string('staff_no', 100);
            $table->string('full_name', 100);
            $table->string('surf_name', 100);
            $table->string('email', 100);
            $table->string('password', 100);
            $table->string('marital_status', 10);
            $table->string('ic_new', 100);
            $table->string('ic_old', 100);
            $table->string('address_permanent', 250);
            $table->string('postcode_permanent', 10);
            $table->integer('district_id_permanent')->unsigned();
            $table->integer('state_id_permanent')->unsigned();
            $table->string('address_correspondence', 250);
            $table->string('postcode_correspondence', 10);
            $table->integer('district_id_correspondence')->unsigned();
            $table->integer('state_id_correspondence')->unsigned();
            $table->date('date_of_birth');
            $table->date('date_joined');
            $table->integer('employment_type_id')->unsigned();
            $table->string('mobile_phone', 50);
            $table->string('office_phone', 50);
            $table->string('extension_no', 100);
            $table->string('home_phone', 50);
            $table->string('status', 20);
            $table->timestamps();

            $table->index('staff_no');
            $table->index('ic_new');
            $table->index('ic_old');
            $table->index('district_id_permanent');
            $table->index('state_id_permanent');
            $table->index('district_id_correspondence');
            $table->index('state_id_correspondence');
            $table->index('employment_type_id');

            $table->foreign('district_id_permanent')->references('id')->on('core_district');
            $table->foreign('district_id_correspondence')->references('id')->on('core_district');
            $table->foreign('employment_type_id')->references('id')->on('core_employment_type');
            $table->foreign('state_id_permanent')->references('id')->on('core_state');
            $table->foreign('state_id_correspondence')->references('id')->on('core_state');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
