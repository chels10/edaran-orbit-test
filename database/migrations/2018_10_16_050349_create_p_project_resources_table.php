<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePProjectResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_project_resources', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('PrjctCode', 100);
            $table->string('UserID', 100);
            $table->string('AssignBy', 100);
            $table->date('StartDate');
            $table->date('UntilDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_project_resources');
    }
}
