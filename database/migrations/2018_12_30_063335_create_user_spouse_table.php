<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSpouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_spouse', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('spouse_name', 200);
            $table->string('spouse_ic', 200);
            $table->string('spouse_tel', 200);
            $table->string('spouse_dob', 200);
            $table->string('spouse_marriage', 200);
            $table->string('spouse_address', 200);
            $table->string('spouse_postcode', 200);
            $table->string('spouse_district', 200);
            $table->string('spouse_state', 200);
            $table->string('spouse_emp', 200);
            $table->string('spouse_emptax', 200);
            $table->string('spouse_empadd', 200);
            $table->string('spouse_emppost', 200);
            $table->string('spouse_empdis', 200);
            $table->string('spouse_empstate', 200);
            $table->string('spouse_emptel', 200);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_spouse');
    }
}
