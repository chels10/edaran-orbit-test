<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoreDeclarationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_declaration', function (Blueprint $table) {
            $table->increments('dec_id');
            $table->string('UserId');
            $table->string('staff_dec_1', 100);
            $table->string('staff_dec_2');
            $table->string('staff_dec_3');

           // $table->foreign('UserId')->references('user_id')->on('users');

            $table->index('UserId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_declaration');
    }
}
