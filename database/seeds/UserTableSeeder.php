<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('App\UserModel');
        DB::table('users')->insert([
            'name' => $faker->sentence,
            'email' => $faker->sentence.'@gmail.com',
            'password' => $faker->sentence,

            'staff_no' => $faker->sentence(),
            'full_name' => $faker->sentence(),
            'surf_name' => $faker->sentence(),
            'marital_status' => $faker->sentence(),


            'password' => bcrypt('password'),
            'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
