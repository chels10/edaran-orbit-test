<?php

use Faker\Generator as Faker;

$factory->define(App\DistrictModel::class, function (Faker $faker) {
    return [
        'district_name' => $faker->streetName,
        'state_id' => '1',
    ];
});
