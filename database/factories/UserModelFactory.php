<?php

use Faker\Generator as Faker;

$factory->define(App\UserModel::class, function (Faker $faker) {
    return [
        'user_id' => '1500',
        'staff_no'=> $faker->randomNumber,
        'full_name'=> $faker->name,
        'surf_name'=> $faker->lastName,
        'email'=> $faker->unique()->safeEmail,
        'password' => '$2y$10$VDrAgyr2KjATZV0C107Qvu6Uov7VapTK2Xo4QFjUUmSdU/M9bUqfq',
        'marital_status'=> 'SINGLE',
        'ic_new'=> '0000-00-0000',
        'ic_old'=> '0000-00-0000',
        'address_permanent'=> $faker->address,
        'postcode_permanent'=> $faker->postcode,
        'district_id_permanent'=> '1',
        'state_id_permanent'=> '1',
        'address_correspondence'=> $faker->address,
        'postcode_correspondence'=> $faker->postcode,
        'district_id_correspondence'=> '1',
        'state_id_correspondence'=> '1',
        'date_of_birth'=> $faker->date,
        'date_joined'=> $faker->date,
        'employment_type_id'=> '1',
        'mobile_phone'=> $faker->randomNumber,
        'office_phone'=> $faker->randomNumber,
        'extension_no'=> $faker->randomNumber,
        'home_phone'=> $faker->randomNumber,
        'status'=> 'ACTIVE',
    ];
});
