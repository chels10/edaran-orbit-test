<?php

use Faker\Generator as Faker;

$factory->define(App\OrganizationUnitModel::class, function (Faker $faker) {
    return [
        'orgunit_name'=> $faker->company, 
        'address'=> $faker->address, 
        'postal_code'=> $faker->randomNumber, 
        'district_id'=> '2', 
        'state_id'=> '2', 
        'country_id'=> '2', 
        'office_phone'=> $faker->randomNumber, 
        'mobile_phone'=> $faker->randomNumber, 
        'fax'=> 'yes', 
        'email'=> $faker->unique()->safeEmail, 
        'remark'=> 'yes bois'
    ];
});
