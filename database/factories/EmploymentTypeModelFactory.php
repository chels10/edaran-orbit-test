<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmploymentTypeModel;
use Faker\Generator as Faker;

$factory->define(EmploymentTypeModel::class, function (Faker $faker) {
    return [
        'employment_type_name' => 'CONTRACT',
        'created_at' => $faker->dateTime,
        'updated_at'=> $faker->dateTime,
    ];
});
