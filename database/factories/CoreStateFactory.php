<?php

use Faker\Generator as Faker;

$factory->define(App\StateModel::class, function (Faker $faker) {
    return [
        'state_name' => $faker->state,
        'country_id' => '1',
    ];
});
