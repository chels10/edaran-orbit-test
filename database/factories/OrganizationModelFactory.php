<?php

use Faker\Generator as Faker;

$factory->define(App\OrganizationModel::class, function (Faker $faker) {
    return [
        'organization_name' => $faker->company, 
        'address' => $faker->address, 
        'postal_code' => $faker->postcode, 
        'district_id'=> '1', 
        'state_id' => '1', 
        'country_id' => '1', 
        'office_phone'=> $faker->randomNumber, 
        'mobile_phone'=> $faker->randomNumber, 
        'fax' => 'yes', 
        'email'=> $faker->unique()->safeEmail, 
        'remark'=> 'yes boi'
    ];
});
