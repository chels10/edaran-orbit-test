<?php

namespace Tests\Feature;

use App\DistrictModel;
use App\StateModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class DistrictTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    /** @test */
    public function test_able_to_add_district()
    {
        $this->withoutExceptionHandling();
        $data = [
            'district_name' => 'ALOR GAJAH test',
            'state_id' => '1'
        ];

        $response = $this->json('PUT', '/api/district/put', $data);
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJson([
                'Response' => '200'
            ]);
    }

    /** @test */
    public function test_able_to_retrieve_all_district()
    {
        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();

        $response = $this->json('GET', '/api/district/retrieveall');
        //dd($response);
        $response
        ->assertStatus(200)
        ->assertJsonStructure([
            'Response',

            'Result' => [
                [
                    'id',
                    'district_name',
                    'state_id',
                    'created_at',
                    'updated_at',
                    'state' => [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]
        ]);
    }

    /** @test */
    public function test_able_to_retrieve_specific_district()
    {
        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();

        $data = [
            'id' => $district->id
        ];

        $response = $this->json('GET', '/api/district/find', $data);
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([

                [
                    'id',
                    'district_name',
                    'state_id',
                    'created_at',
                    'updated_at',
                    'statename',
                    'state' => [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }

    /** @test */
    public function test_able_to_update_district()
    {
        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();

        $data = [
            'id' => $district->id,
            'district_name' => 'ALOR GAJAH test',
            'state_id' => '1'
        ];

        $response = $this->json('POST', '/api/district/update', $data);
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJson([
                'Response' => '200'
            ]);
    }

    /** @test */
    public function test_able_to_delete_district()
    {
        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();

        $data = [
            'id' => $district->id,
        ];

        $response = $this->json('DELETE', '/api/district/destroy', $data);
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJson([
                'Response' => '200'
            ]);
    }
    
}
