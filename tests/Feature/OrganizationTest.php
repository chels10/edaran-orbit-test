<?php

namespace Tests\Feature;

use App\OrganizationModel;
use Dotenv\Regex\Result;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrganizationTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    /** @test */
    public function able_to_add_organization_test()
    {
        $data = [
            'organization_name' => "Rambutan.test2",
            'address' => "0",
            'postal_code' => "70450",
            'district_id' => "0",
            'state_id' => "0",
            'country_id' => "0",
            'office_phone' => "06-6773577",
            'mobile_phone' => "017-3225950",
            'fax' => "06-6773577",
            'email' => "rambutantest@gmail.com",
            'remark' => "Rambutan testing"
        ];

        $response = $this->json('PUT', '/api/organization/put', $data);
        //dd($response);
        $response->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
    }

    /** @test */
    public function able_to_retrieve_all_organization_details_test()
    {
        $this->withoutExceptionHandling();

        $organization = factory(OrganizationModel::class)->create();

        $response = $this->json('GET', '/api/organization/retrieveall');
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Response',

                'Result' => [
                    [
                        'id',
                        'organization_name',
                        'address',
                        'postal_code',
                        'district_id',
                        'state_id',
                        'country_id',
                        'office_phone',
                        'mobile_phone',
                        'fax',
                        'email',
                        'remark',
                        'created_at',
                        'updated_at'
                    ]
                ]

            ]);
    }

    /** @test */
    public function test_able_to_retrieve_specific_organization()
    {
        $this->withoutExceptionHandling();

        $organization = factory(OrganizationModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $organization->id,
        ];

        //dd($data);

        $find = $this->json('GET', '/api/organization/find', $data);
        //dd($find);
        $find->assertStatus(200);
        $find->assertJsonStructure([
            'Response',

            'Result' => [
                'id',
                'organization_name',
                'address',
                'postal_code',
                'district_id',
                'state_id',
                'country_id',
                'office_phone',
                'mobile_phone',
                'fax',
                'email',
                'remark',
                'created_at',
                'updated_at'

            ]
        ]);
    }

    /** @test */
    public function test_able_to_update_organization()
    {
        $this->withoutExceptionHandling();

        $organization = factory(OrganizationModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $organization->id,
            'organization_name' => 'Test Update',
            'address' => 'Test Update',
            'postal_code' => '1',
            'district_id' => '1',
            'state_id' => '1',
            'country_id' => '1',
            'office_phone' => '1',
            'mobile_phone' => '1',
            'fax' => '1',
            'email' => '1',
            'remark' => '1',
        ];

        //dd($data);

        $response = $this->json('POST', '/api/organization/update', $data);
        //dd($find);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }


    /** @test */
    public function able_to_delete_organization_test()
    {
        $this->withoutExceptionHandling();

        $organization = factory(OrganizationModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $organization->id,
        ];

        //dd($data);

        $response = $this->json('DELETE', '/api/organization/destroy', $data);
        //dd($find);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }
}
