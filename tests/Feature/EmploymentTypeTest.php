<?php

namespace Tests\Feature;

use App\EmploymentTypeModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmploymentTypeTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    /**
     *
     * @test */
    public function able_to_add_employment_type_test()
    {
        $data = [
            'employment_type_name' => "Test"
        ];
        $response = $this->json('PUT', '/api/employmenttype/put', $data);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }

    /** @test */
    public function able_to_retrieve_all_employment_type_details_test()
    {
        $this->withoutExceptionHandling();

        $employmenttype = factory(EmploymentTypeModel::class)->create();
        $response = $this->json('GET', '/api/employmenttype/retrieveall');
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'employment_type_name',
                    'created_at',
                    'updated_at'
                ]

            ]);
    }

    /** @test */
    public function test_able_to_retrieve_specific_employment_type()
    {
        $this->withoutExceptionHandling();

        $employmenttype = factory(EmploymentTypeModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $employmenttype->id,
        ];

        //dd($data);

        $find = $this->json('GET', '/api/employmenttype/find', $data);
        //dd($find);
        $find->assertStatus(200);
        $find->assertJsonStructure([
            'Response',

            'Result' => [
                'id',
                'employment_type_name',
                'created_at',
                'updated_at'

            ]
        ]);
    }

    /** @test */
    public function test_able_to_update_employment_type()
    {
        $this->withoutExceptionHandling();

        $employmenttype = factory(EmploymentTypeModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $employmenttype->id,
            'employment_type_name' => 'Update',

        ];

        //dd($data);

        $response = $this->json('POST', '/api/employmenttype/update', $data);
        //dd($find);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }


    /** @test */
    public function able_to_delete_employment_type_test()
    {
        $this->withoutExceptionHandling();

        $employmenttype = factory(EmploymentTypeModel::class)->create();

        //echo "cmon", $user->user_id;

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'id' => $employmenttype->id,
        ];

        //dd($data);

        $response = $this->json('DELETE', '/api/employmenttype/destroy', $data);
        //dd($find);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }
}
