<?php

namespace Tests\Feature;

use App\UserModel;
use App\StateModel;
use App\DistrictModel;
use App\EmploymentTypeModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    /** @test */
    public function able_to_add_user_test()
    {

        //create state
        //create district
        //create employment type

        $this->withoutExceptionHandling();

        $data = [
            'user_id' => "ECOM0001",
            'staff_no' => "544",
            'full_name' => "rambutan test",
            'surf_name' => "rambutan",
            'email' => "rambutan@test.test",
            'password' => "rambutan",
            'marital_status' => "MARRIED",
            'ic_new' => "999999-99-9999",
            'ic_old' => "830713-05-5339",
            'address_permanent' => "10-4-3 BLOK B KELUMPUK SUNDING MALAM JALAN 20/56, AU3 TAMAN KERAMAT WANGSA,",
            'postcode_permanent' => "54200",
            'district_name' => "SETIAWANGSA",
            'state_name' => " W.P KUALA LUMPUR",
            'address_correspondence' => "10-4-3 BLOK B KELUMPUK SUNDING MALAM JALAN 20/56, AU3 TAMAN KERAMAT WANGSA,",
            'postcode_correspondence' => "54200",
            'district_name_correspondence' => "SETIAWANGSA",
            'state_name_correspondence' => "W.P KUALA LUMPUR",
            'date_of_birth' => "1000-01-01",
            'date_joined' => "1000-01-01",
            'employment_type_id' => "1",
            'mobile_phone' => "016-6224734",
            'office_phone' => "1",
            'extension_no' => "262",
            'home_phone' => "000",
            'district_id_permanent' => "1",
            'state_id_permanent' => "1",
            'district_id_correspondence' => "1",
            'state_id_correspondence' => "1",
            'status' => "ACTIVE",

        ];


        $response = $this->json('PUT', '/api/user/put', $data);
        //dd($response);

        $response->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
    }

    /** @test */
    public function able_to_retrieve_all_users_details()
    {
        $this->withoutExceptionHandling();

        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();
        $employmenttype = factory(EmploymentTypeModel::class)->create();
        //dd($employmenttype);

        $user = factory(UserModel::class)->create();

        $response = $this->json('GET', '/api/user/retrieveall');
        //dd($response);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'user_id',
                    'staff_no',
                    'full_name',
                    'surf_name',
                    'email',
                    'password',
                    'marital_status',
                    'ic_new',
                    'ic_old',
                    'address_permanent',
                    'postcode_permanent',
                    'district_id_permanent',
                    'state_id_permanent',
                    'address_correspondence',
                    'postcode_correspondence',
                    'district_id_correspondence',
                    'state_id_correspondence',
                    'date_of_birth',
                    'date_joined',
                    'employment_type_id',
                    'mobile_phone',
                    'office_phone',
                    'extension_no',
                    'home_phone',
                    'status',
                    'created_at',
                    'updated_at',
                    'dpermanent',
                    'spermanent',

                    'dcorrespondence' => [
                        'id',
                        'district_name',
                        'state_id',
                        'created_at',
                        'updated_at'
                    ],

                    'scorrespondence' => [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ],

                    'district_permanent' => [
                        'id',
                        'district_name',
                        'state_id',
                        'created_at',
                        'updated_at'
                    ],

                    'state_permanent' => [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ],

                    'district_correspondence' => [
                        'id',
                        'district_name',
                        'state_id',
                        'created_at',
                        'updated_at'
                    ],

                    'state_correspondence' => [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ],

                ]

            ]);
    }

    /** @test */
    /**public function able_to_retrieve_specific_user_details_test()
    {
        $this->withoutExceptionHandling();

        $state = factory(StateModel::class)->create();

        $district = factory(DistrictModel::class)->create();
        $employmenttype = factory(EmploymentTypeModel::class)->create();
        $user = factory(UserModel::class)->create();

        $response = $this->json('GET', '/api/user/retrieveall');
        $response -> assertStatus(200);
        //echo "cmon", $user->user_id;
        
        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($response);

        $data = [
            'user_id' => $user->user_id,
        ];

        //dd($data);

        $find = $this->json('GET', '/api/user/find',$data);
        //dd($find);
        $find->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
        
    }*/

    /** @test */
    public function able_to_update_users_test()
    {
        $this->withoutExceptionHandling();

        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();
        $employmenttype = factory(EmploymentTypeModel::class)->create();
        $user = factory(UserModel::class)->create();

        //$n = $user->user_id;
        //$array = array($n);
        //echo"here ass:", implode($array);
        //dd($user);

        $data = [
            'id' => $user->id,
            'user_id' => "EC009",
            'staff_no' => "544",
            'full_name' => "rambutan test update",
            'surf_name' => "rambutan",
            'email' => "rambutan@test.test",
            'password' => "rambutan",
            'marital_status' => "MARRIED",
            'ic_new' => "999999-99-9999",
            'ic_old' => "830713-05-5339",
            'address_permanent' => "10-4-3 BLOK B KELUMPUK SUNDING MALAM JALAN 20/56, AU3 TAMAN KERAMAT WANGSA,",
            'postcode_permanent' => "54200",
            'district_name' => "SETIAWANGSA",
            'state_name' => " W.P KUALA LUMPUR",
            'address_correspondence' => "10-4-3 BLOK B KELUMPUK SUNDING MALAM JALAN 20/56, AU3 TAMAN KERAMAT WANGSA,",
            'postcode_correspondence' => "54200",
            'district_name_correspondence' => "SETIAWANGSA",
            'state_name_correspondence' => "W.P KUALA LUMPUR",
            'employment_type_id' => "1",
            'mobile_phone' => "016-6224734",
            'office_phone' => "1",
            'extension_no' => "262",
            'home_phone' => "000",
            'district_id_permanent' => "1",
            'state_id_permanent' => "1",
            'district_id_correspondence' => "1",
            'state_id_correspondence' => "1",
            'status' => "ACTIVE",
            'date_of_birth' => "1000-01-01",
            'date_joined' => "1000-01-01",
        ];

        $response = $this->json('POST', '/api/user/update', $data);
        //dd($data);
        $response->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
    }

    /** @test */
    public function able_to_delete_users_test()
    {
        $this->withoutExceptionHandling();

        $state = factory(StateModel::class)->create();
        $district = factory(DistrictModel::class)->create();
        $employmenttype = factory(EmploymentTypeModel::class)->create();
        $user = factory(UserModel::class)->create();

        //dd($user);
        $data = [
            'id' => $user->id
        ];

        $delete = $this->json('DELETE', '/api/user/destroy', $data);

        $delete->assertStatus(200);
        $delete->assertJson([
            'Response' => '200'
        ]);
    }
}
