<?php

namespace Tests\Feature;

use App\StateModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class CoreStateTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;
    /** @test */
    public function test_able_to_add_core_state()
    {
        //$state = factory(StateModel::class)->create();
        //dd($state);
        $data = [
            'state_name' => "Minnesota",
            'country_id' => "1"
        ];

        $response = $this->json('PUT', '/api/state/put', $data);
        //dd($response);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }

    /** @test */
    public function test_able_to_retrieve_all_core_state()
    {
        $state = factory(StateModel::class)->create();
        //dd($state);

        $response = $this->json('GET', '/api/state/retrieveall');
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Response',
                'Result' => [
                    [
                        'id',
                        'state_name',
                        'country_id',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }

    /** @test */
    public function test_able_to_retrieve_specific_core_state()
    {
        $state = factory(StateModel::class)->create();

        $data = [
            'id' => $state->id
        ];

        $response = $this->json('GET', '/api/state/find', $data);
        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Response',
                'Result' => [
                    'id',
                    'state_name',
                    'country_id',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /** @test */
    public function test_able_to_update_core_state()
    {
        $state = factory(StateModel::class)->create();

        $data = [
            'id' => $state->id,
            'state_name' => 'State Update',
            'country_id' => '1'
        ];

        $response = $this->json('POST', '/api/state/update', $data);
        //dd($response);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }

    /** @test */
    public function test_able_to_delete_core_state()
    {
        $state = factory(StateModel::class)->create();

        $data = [
            'id' => $state->id,
        ];

        $response = $this->json('DELETE', '/api/state/destroy', $data);
        //dd($response);
        $response
            ->assertJson([
                'Response' => '200'
            ]);
    }
}
