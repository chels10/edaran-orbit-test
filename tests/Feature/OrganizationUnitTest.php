<?php

namespace Tests\Feature;

use App\OrganizationUnitModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class OrganizationUnitTest extends TestCase
{
    use WithFaker;
    use DatabaseMigrations;

    /** @test */
    public function able_to_add_organization_unit_test()
    {
        $data = [
            'orgunit_name' => "Rambutan.test3",
            'address' => "yes chel",
            'postal_code' => "70450",
            'district_id' => "2",
            'state_id' => "2",
            'country_id' => "2",
            'office_phone' => "06-6773577",
            'mobile_phone' => "017-3225950",
            'fax' => "06-6773577",
            'email' => "rambutantest@gmail.com",
            'remark' => "Rambutan testing"
        ];

        $response = $this->json('PUT', '/api/organizationunit/put', $data);
        //dd($response);
        $response->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
    }

    /** @test */
    public function able_to_retrieve_all_organization_unit_details_test()
    {
        $this->withoutExceptionHandling();
        factory(OrganizationUnitModel::class)->create();

        $response = $this->json('GET', '/api/organizationunit/retrieveall');

        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([

                [
                    'orgunit_code',
                    'orgunit_name',
                    'address',
                    'postal_code',
                    'district_id',
                    'state_id',
                    'country_id',
                    'office_phone',
                    'mobile_phone',
                    'fax',
                    'email',
                    'remark',
                    'created_at',
                    'updated_at'
                ]

            ]);
    }

    /** @test */
    public function test_able_to_retrieve_specific_organization_unit()
    {
        $this ->withoutExceptionHandling();
        $orgunit = factory(OrganizationUnitModel::class)->create();

        $data = [
            'orgunit_code' => $orgunit->orgunit_code
        ];

        $response = $this->json('GET', '/api/organizationunit/find', $data);

        //dd($response);
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'Response',

                'Result' => [
                    'orgunit_code',
                    'orgunit_name',
                    'address',
                    'postal_code',
                    'district_id',
                    'state_id',
                    'country_id',
                    'office_phone',
                    'mobile_phone',
                    'fax',
                    'email',
                    'remark',
                    'created_at',
                    'updated_at'
                ]

            ]);
    }

    /** @test */
    public function test_able_to_update_organization_unit()
    {
        $this->withoutExceptionHandling();

        $orgunit = factory(OrganizationUnitModel::class)->create();

        $data = [
            'orgunit_code' => $orgunit->orgunit_code,
            'orgunit_name' => "Rambutan.test3",
            'address' => "yes chel",
            'postal_code' => "70450",
            'district_id' => "2",
            'state_id' => "2",
            'country_id' => "2",
            'office_phone' => "06-6773577",
            'mobile_phone' => "017-3225950",
            'fax' => "06-6773577",
            'email' => "rambutantest@gmail.com",
            'remark' => "Rambutan testing"
        ];

        $response = $this->json('PUT', '/api/organizationunit/put', $data);
        //dd($response);
        $response->assertStatus(200);
        $response->assertJson([
            'Response' => '200'
        ]);
    }

    /** @test */
    public function able_to_delete_organization_unit_test()
    {
        $this->withoutExceptionHandling();

        $orgunit = factory(OrganizationUnitModel::class)->create();

        $data = [
            'orgunit_code' => $orgunit->orgunit_code
        ];

        $delete = $this->json('DELETE', '/api/organizationunit/destroy' , $data);
        //dd($delete);
        $delete->assertStatus(200);
        $delete->assertJson([
            'Response' => '200'
        ]);
    }
}
